{
    "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
    "@id": "http://schema.org/Place",
    "name": "Place",
    "description": "Entities that have a somewhat fixed, physical extension.",
    "title": "Place",
    "abstract": false,
    "sealed": false,
    "implements": "Thing",
    "classPath": "./models/place-model",
    "version": "1.1",
    "fields": [

        {
            "@id": "http://schema.org/events",
            "name": "events",
            "title": "events",
            "description": "Upcoming or past events associated with this place or organization.",
            "type": "Event"
        },
        {
            "@id": "http://schema.org/globalLocationNumber",
            "name": "globalLocationNumber",
            "title": "globalLocationNumber",
            "description": "The <a href=\"http://www.gs1.org/gln\">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/maximumAttendeeCapacity",
            "name": "maximumAttendeeCapacity",
            "title": "maximumAttendeeCapacity",
            "description": "The total number of individuals that may attend an event or venue.",
            "type": "Integer"
        },

        {
            "@id": "http://schema.org/map",
            "name": "map",
            "title": "map",
            "description": "A URL to a map of the place.",
            "type": "URL"
        },
        {
            "@id": "http://schema.org/branchCode",
            "name": "branchCode",
            "title": "branchCode",
            "description": "A short textual code (also called \"store code\") that uniquely identifies a place of business. The code is typically assigned by the parentOrganization and used in structured URLs.</p>\n\n<p>For example, in the URL http://www.starbucks.co.uk/store-locator/etc/detail/3047 the code \"3047\" is a branchCode for a particular branch.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/address",
            "name": "address",
            "title": "address",
            "description": "Physical address of the item.",
            "type": "PostalAddress"
        },
        {
            "@id": "http://schema.org/logo",
            "name": "logo",
            "title": "logo",
            "description": "An associated logo.",
            "type": "URL"
        },
        {
            "@id": "http://schema.org/telephone",
            "name": "telephone",
            "title": "telephone",
            "description": "The telephone number.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/geo",
            "name": "geo",
            "title": "geo",
            "description": "The geo coordinates of the place.",
            "type": "GeoCoordinates"
        },
        {
            "@id": "http://schema.org/containedInPlace",
            "name": "containedInPlace",
            "title": "containedInPlace",
            "description": "The basic containment relation between a place and one that contains it.",
            "type": "Place"
        },
        {
            "@id": "http://schema.org/publicAccess",
            "name": "publicAccess",
            "title": "publicAccess",
            "description": "A flag to signal that the <a class=\"localLink\" href=\"http://schema.org/Place\">Place</a> is open to public visitors.  If this property is omitted there is no assumed default boolean value",
            "type": "Boolean"
        },
        {
            "@id": "http://schema.org/faxNumber",
            "name": "faxNumber",
            "title": "faxNumber",
            "description": "The fax number.",
            "type": "Text"
        },
        {
            "@id": "http://schema.org/isAccessibleForFree",
            "name": "isAccessibleForFree",
            "title": "isAccessibleForFree",
            "description": "A flag to signal that the item, event, or place is accessible for free.",
            "type": "Boolean"
        },
        {
            "name": "displayCode",
            "title": "Display code",
            "description": "The display code of the place",
            "type": "Text",
            "size": 15
        },
        {
            "name": "departments",
            "title": "Departments",
            "description": "A collection of related departments.",
            "type": "Department",
            "mapping": {
                "parentModel": "Department",
                "parentField": "id",
                "childModel": "Place",
                "childField": "id",
                "associationType": "junction",
                "privileges": [
                    {
                        "mask": 1,
                        "type": "global",
                        "account": "*"
                    },
                    {
                        "mask": 15,
                        "type": "global"
                    },
                    {
                        "mask": 15,
                        "type": "global",
                        "account": "Administrators"
                    },
                    {
                        "mask": 15,
                        "type": "self",
                        "account": "Registrar",
                        "filter": "parentId eq departments()"
                    }
                ]
            }
        }
    ],
    "eventListeners": [
        {
            "type": "./listeners/before-save-place-listener"
        }
    ],
    "privileges": [
        {
            "mask": 1,
            "type": "global",
            "account": "*"
        },
        {
            "mask": 31,
            "type": "global"
        },
        {
            "mask": 15,
            "type": "global",
            "account": "Administrators"
        },
        {
            "mask": 4,
            "type": "global",
            "account": "Registrar"
        }
    ]
}
