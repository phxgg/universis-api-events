import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 
 * @property {number} id
 * @property {User} user
 * @property {EventHoursSpecification|any} eventHoursSpecification
 * @property {boolean} conditional
 * @augments {DataObject}
 */
@EdmMapping.entityType('PersonAvailability')
class PersonAvailability extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = PersonAvailability;
